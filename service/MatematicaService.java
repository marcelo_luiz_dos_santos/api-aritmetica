package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n: entradaDTO.getNumeros()) {
            numero += n;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int numero = 0;
        int i = 0;
        for (int n : entradaDTO.getNumeros()) {
            if (i == 0){
                numero = n;
                i++;
            }else {
                numero *= n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = 0;
        int i = 0;
        for (int n : entradaDTO.getNumeros()) {
            if (i == 0){
                numero = n;
                i++;
            }else {
                numero -= n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int numero = 0;
        int i = 0;
        for (int n : entradaDTO.getNumeros()) {
            if (i == 0){
                numero = n;
                i++;
            }else {
                numero /= n;
            }
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }
}

